# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: 2024 KDE e.V.

# see windowsbinariessigner-projects.sample.yaml in sysadmin/ci-notary-service for details

defaults:
  branches: []

##########################################################################################################
# The entries are sorted by their project name (i.e. the name of the directory for the project on invent)
##########################################################################################################

# First the ci-management repo for signing the caches

sysadmin/ci-management:
  branches:
    master:

# Now the apps

office/alkimia:
  branches:
    master:

network/alligator:
  branches:
    master:

utilities/ark:
  applicationid: KDEe.V.Ark
  branches:
    master:
    release/24.12:
    release/25.04:

education/blinken:
  branches:
    master:

games/bomber:
  branches:
    master:

games/bovo:
  branches:
    master:

office/calligra:
  branches:
    master:

office/calligraplan:
  branches:
    master:
    3.3:

education/cantor:
  applicationid: KDEe.V.Cantor
  branches:
    master:
    release/24.12:
    release/25.04:

office/crow-translate:
  branches:
    master:
    release/3.0:

graphics/digikam:
  applicationid: KDEe.V.digiKam
  branches:
    master:

graphics/krita:
  branches:
    master:
    krita/5.2:
    release/5.2.9:

system/dolphin:
  branches:
    master:
    release/24.12:
    release/25.04:

multimedia/elisa:
  applicationid: KDEe.V.Elisa
  branches:
    master:
    release/24.12:
    release/25.04:

utilities/filelight:
  applicationid: KDEe.V.Filelight
  branches:
    master:
    release/24.12:
    release/25.04:

graphics/glaxnimate:
  applicationid: KDEe.V.47488D4059B84
  branches:
    master:
    release/0.6:

games/granatier:
  branches:
    master:

multimedia/haruna:
  branches:
    master:

utilities/isoimagewriter:
  applicationid: KDEe.V.ISOImageWriter
  branches:
    master:

network/kaidan:
  branches:
    master:

education/kalgebra:
  branches:
    master:

games/kapman:
  branches:
    master:

multimedia/kasts:
  applicationid: KDEe.V.Kasts
  branches:
    master:
    release/24.12:
    release/25.04:

utilities/kate:
  applicationid: KDEe.V.Kate
  branches:
    master:
    release/24.12:
    release/25.04:

games/katomic:
  branches:
    master:

office/kbibtex:
  branches:
    master:

games/kblackbox:
  branches:
    master:

games/kblocks:
  branches:
    master:

education/kbruch:
  branches:
    master:

games/kbounce:
  branches:
    master:

games/kbreakout:
  branches:
    master:

multimedia/kdenlive:
  branches:
    master:
    release/24.12:
    release/25.04:

network/kdeconnect-kde:
  applicationid: KDEe.V.KDEConnect
  branches:
    master:
    release/24.12:
    release/25.04:

kdevelop/kdevelop:
  branches:
    master:
    release/24.12:
    release/25.04:

games/kdiamond:
  branches:
    master:

sdk/kdiff3:
  applicationid: KDEe.V.KDiff3
  branches:
    master:
    1.11:
    1.12:

office/kexi:
  branches:
    master:
    3.2:

games/kfourinline:
  branches:
    master:

education/kgeography:
  branches:
    master:

games/kgoldrunner:
  branches:
    master:

multimedia/kid3:
  branches:
    master:

education/kig:
  branches:
    master:

games/kigo:
  branches:
    master:

office/kile:
  applicationid: KDEe.V.Kile
  branches:
    master:

games/killbots:
  branches:
    master:

sdk/kirigami-gallery:
  branches:
    master:

games/kiriki:
  branches:
    master:

education/kiten:
  applicationid: KDEe.V.Kiten
  branches:
    master:
    release/24.12:
    release/25.04:

games/kjumpingcube:
  branches:
    master:

games/klickety:
  branches:
    master:

games/klines:
  branches:
    master:

games/kmahjongg:
  branches:
    master:

games/kmines:
  branches:
    master:

education/kmplot:
  branches:
    master:

office/kmymoney:
  branches:
    master:
    5.1:

games/knavalbattle:
  branches:
    master:

games/knetwalk:
  branches:
    master:

games/kolf:
  branches:
    master:

games/kollision:
  branches:
    master:

graphics/kolourpaint:
  branches:
    master:

utilities/konsole:
  applicationid: KDEe.V.Konsole
  branches:
    master:

games/konquest:
  branches:
    master:

network/konversation:
  branches:
    master:
    release/24.12:
    release/25.04:

games/kpat:
  branches:
    master:

games/kreversi:
  branches:
    master:

utilities/kronometer:
  branches:
    master:

games/kshisen:
  branches:
    master:

games/ksnakeduel:
  branches:
    master:

games/kspaceduel:
  branches:
    master:

games/ksquares:
  branches:
    master:

education/kstars:
  applicationid: KDEe.V.KStars
  branches:
    master:
    stable-3.7.5:

games/ksudoku:
  branches:
    master:

system/ksystemlog:
  branches:
    master:

pim/ktimetracker:
  branches:
    master:

utilities/ktrip:
  applicationid: KDEe.V.KTrip
  branches:
    master:
    release/24.12:
    release/25.04:

games/ktuberling:
  branches:
    master:

education/kturtle:
  branches:
    master:

games/kubrick:
  branches:
    master:

education/kwordquiz:
  branches:
    master:

education/labplot:
  applicationid: KDEe.V.LabPlot
  branches:
    master:
    release/2.10:

sdk/lokalize:
  applicationid: KDEe.V.Lokalize
  branches:
    master:
    release/24.12:
    release/25.04:

games/lskat:
  branches:
    master:

education/marble:
  branches:
    master:

office/marknote:
  branches:
    master:

pim/merkuro:
  branches:
    master:

education/minuet:
  branches:
    master:

network/neochat:
  applicationid: KDEe.V.NeoChat
  branches:
    master:
    release/24.12:
    release/25.04:

utilities/okteta:
  branches:
    master:

graphics/okular:
  applicationid: KDEe.V.Okular
  branches:
    master:
    release/24.12:
    release/25.04:

education/parley:
  branches:
    master:

graphics/peruse:
  branches:
    master:

games/picmi:
  branches:
    master:

education/rkward:
  applicationid: KDEe.V.RKWard
  branches:
    master:
    releases/0.8.0:

network/ruqola:
  applicationid: KDEe.V.Ruqola
  branches:
    master:
    2.4:

pim/kontact:
  branches:
    master:

pim/kalarm:
  branches:
    master:

pim/akregator:
  branches:
    master:

pim/pim-sieve-editor:
  branches:
    master:

libraries/snoretoast:
  branches:
    master:

office/skrooge:
  branches:
    master:

education/step:
  branches:
    master:
    release/24.12:
    release/25.04:

office/tellico:
  branches:
    master:

network/tokodon:
  applicationid: KDEe.V.Tokodon
  branches:
    master:
    release/24.12:
    release/25.04:

sdk/umbrello:
  branches:
    master:
    release/24.12:
    release/25.04:

pim/vakzination:
  branches:
    master:

graphics/karp:
  branches:
    master:
