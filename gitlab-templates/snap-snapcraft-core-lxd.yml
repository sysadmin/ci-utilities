include:
  - /gitlab-templates/blocks/snap-base.yml
  - /gitlab-templates/blocks/workflow.yml

snap_snapcraft_core_lxd:
  extends: .snap_base
  stage: deploy
  inherit:
    default: true
  before_script:
    # double check  lxd instances that can pollute subsequent rebuilds in the persistent vm have been cleaned up
    # being snapcraft, of course different builds are in different projects, work around that with an if else ...
    - |
      if [ "$SNAP_BUILD_TYPE" == "legacy" ]; then
        # firstly remove any snapcraft-created LXD containers
        for c in $(lxc storage info default | awk '{print $2}' | grep snapcraft-);
                do lxc delete --force $c
        done
        # then remove any snapcraft-created LXD base-instances
        for d in $(lxc storage info default | awk '{print $2}' | grep base-instance-snapcraft-);
                do lxc delete --force $d
        done
      else
        # firstly remove any snapcraft-created LXD containers
        for c in $(lxc storage info default | awk '{print $2}' | grep snapcraft-);
                do lxc delete --force $c --project snapcraft
        done
        # then remove any snapcraft-created LXD base-instances
        for d in $(lxc storage info default | awk '{print $2}' | grep base-instance-snapcraft-);
                do lxc delete --force $d --project snapcraft
        done
      fi
    # if we are building the kde-qt6-core-sdk snap on the current emberi vm,  ensure that lxc utilises ony 12 of the
    # available 16 cores otherwise qt-webengine will OOM.  this will need to be altered once ephemeral vm's are a thing
    - |
      if [ "$KDE_SNAP_MODULE_NAME" == "kde-qt6-core-sdk" ]; then
        lxc profile set default --project snapcraft limits.cpu=12
      fi
  script:
    # clean
    - snapcraft clean
    # Force usage of LXD for building Snaps
    - echo building $KDE_SNAP_MODULE_NAME with LXD...
    - snapcraft --use-lxd --verbose 2>&1 | tee task.log
    # Make the artifacts dir
    - mkdir -p $CI_PROJECT_DIR/kde-ci-packages/
    # copy the snap to the artifacts dir
    - cp -vf ./*.snap  $CI_PROJECT_DIR/kde-ci-packages/
    - echo $KDE_SNAP_BRANCH >> deploy.env
  after_script:
    # clean up lxd instances that can pollute subsequent rebuilds in the persistent vm
    # being snapcraft, of course different builds are in different projects, work around that with an if else ...
    - |
      if [ "$SNAP_BUILD_TYPE" == "legacy" ]; then
        # firstly remove any snapcraft-created LXD containers
        for c in $(lxc storage info default | awk '{print $2}' | grep snapcraft-);
                do lxc delete --force $c
        done
        # then remove any snapcraft-created LXD base-instances
        for d in $(lxc storage info default | awk '{print $2}' | grep base-instance-snapcraft-);
                do lxc delete --force $d
        done
      else
        # firstly remove any snapcraft-created LXD containers
        for c in $(lxc storage info default | awk '{print $2}' | grep snapcraft-);
                do lxc delete --force $c --project snapcraft
        done
        # then remove any snapcraft-created LXD base-instances
        for d in $(lxc storage info default | awk '{print $2}' | grep base-instance-snapcraft-);
                do lxc delete --force $d --project snapcraft
        done
      fi
  allow_failure: true
  interruptible: true

snap_snapcraft_core_publish:
  extends: .snap_publish
  stage: publish
  needs: [snap_snapcraft_core_lxd]
  script:
    - export KDE_SNAP_BRANCH=$(cat deploy.env)
    - |
      # set the channel for the snap dependent on which branch we are building
      # master = edge
      if [ "$KDE_SNAP_BRANCH" == "master" ]; then
        export KDE_SNAP_CHANNEL="edge"
      # work.core24 = beta
      elif [ "$KDE_SNAP_BRANCH" == "work.core24" ]; then
        export KDE_SNAP_CHANNEL="beta"
      # release.candidate = candidate
      elif [ "$KDE_SNAP_BRANCH" == "release.candidate" ]; then
        export KDE_SNAP_CHANNEL="candidate"
      # release.stable = stable
      elif [ "$KDE_SNAP_BRANCH" == "release.stable" ]; then
        export KDE_SNAP_CHANNEL="stable"
      else
      # don't publish anything else either
        export KDE_SNAP_CHANNEL="no_publish"
        echo "we only publish recognised branches to the snap store"
      fi
      # publish if we are on a recognised branch
      if [ "$KDE_SNAP_CHANNEL" != "no_publish" ]; then
        echo "KDE_SNAP_BRANCH is $KDE_SNAP_BRANCH thus KDE_SNAP_CHANNEL is publishing to $KDE_SNAP_CHANNEL"
        snapcraft push --release=$KDE_SNAP_CHANNEL *.snap --verbose 2>&1 | tee publish.log
      fi
  allow_failure: true
  interruptible: true
